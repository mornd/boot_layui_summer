package com.mornd.utils;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.HashMap;

/**
 * @author mornd
 * @date 2020/12/28 - 16:00
 */
@Deprecated
public class VoiceResult extends HashMap<String,Object> {
    public static VoiceResult success(){
        return success("成功!");
    }

    public static VoiceResult success(String message){
        VoiceResult voiceResult = new VoiceResult();
        voiceResult.setSuccess(true);
        voiceResult.setMessage(message);
        voiceResult.setCode(0);
        return voiceResult;
    }

    public static VoiceResult failure(String message){
        VoiceResult voiceResult = new VoiceResult();
        voiceResult.setSuccess(true);
        voiceResult.setMessage(message);
        voiceResult.setCode(0);
        return voiceResult;
    }
    public VoiceResult setSuccess(Boolean success){
        if(success != null) {
            put("success",success);
        }
        return this;
    }

    public VoiceResult setMessage(String message){
        if(message != null) {
            put("message",message);
        }
        return this;
    }

    public VoiceResult setData(Object data){
        if(data != null) {
            put("data",data);
        }
        return this;
    }

    public VoiceResult setCode(Object code){
        if(code != null) {
            put("code",code);
        }
        return this;
    }
}
