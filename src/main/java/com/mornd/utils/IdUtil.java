package com.mornd.utils;

import java.util.UUID;

/**
 * @author mornd
 * @date 2021/1/5 - 15:48
 * UUID工具类
 */
public class IdUtil {
    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }
}
