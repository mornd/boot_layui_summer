package com.mornd.utils;

import com.baidu.aip.speech.AipSpeech;
import com.baidu.aip.speech.TtsResponse;
import com.baidu.aip.util.Util;
import com.mornd.consts.ApiConst;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author mornd
 * @date 2020/12/28 - 15:08
 */
@Slf4j
@Deprecated
public class BaiduAiUtil {
    /**
     * 语音识别
     * @return
     */
    public static String voiceResolver(String code, HashMap<String,Object> options){
        // 初始化一个AipSpeech
        AipSpeech client = new AipSpeech(ApiConst.BAIDU_APP_ID, ApiConst.BAIDU_API_KEY, ApiConst.BAIDU_SECRET_KEY);

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 可选：设置代理服务器地址, http和socket二选一，或者均不设置
        /*client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
        client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理*/

        /*
        // 可选：设置log4j日志输出格式，若不设置，则使用默认配置
        // 也可以直接通过jvm启动参数设置此环境变量
        System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");
        */

        // 调用接口
        TtsResponse res = client.synthesis(code, "zh", 1, options);
        byte[] data = res.getData();
        JSONObject res1 = res.getResult();
        String result = null;
        if (data != null) {
            try {
                //Util.writeBytesToFileSystem(data, "output.mp3");
                result = new BASE64Encoder().encode(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (res1 != null) {
            log.info("百度语音api调用结果：" + res1.toString(2));
        }
        return result;
    }
}
