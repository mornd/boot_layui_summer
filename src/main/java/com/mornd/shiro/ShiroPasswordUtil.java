package com.mornd.shiro;

import com.mornd.consts.ShiroConst;
import com.mornd.entity.LoginUser;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * @author mornd
 * @date 2021/1/5 - 14:51
 * shiro加密工具类
 */

public class ShiroPasswordUtil {
    /**
     * MD5加密用户密码
     * @param user
     */
    public static void encryptPassword(LoginUser user) {
        // 给user设置盐值
        user.setSalt(new SecureRandomNumberGenerator().nextBytes().toHex());
        // 将用户的注册密码经过散列算法替换成一个不可逆的新密码保存进数据
        //1、采用算法2、原密码3、盐值4、散列次数
        String newPassword = new SimpleHash(ShiroConst.HASH_ALGORITHM_NAME, user.getPassword(),
                ByteSource.Util.bytes(user.getSalt()), ShiroConst.HASH_ITERATIONS).toHex();
        user.setPassword(newPassword);
    }
}
