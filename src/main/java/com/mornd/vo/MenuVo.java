package com.mornd.vo;

import com.mornd.entity.Menu;
import com.mornd.entity.Role;
import lombok.Data;

/**
 * @author mornd
 * @date 2021/1/10 - 21:40
 */
@Data
public class MenuVo extends Menu {
    private Integer page;
    private Integer limit;
}
