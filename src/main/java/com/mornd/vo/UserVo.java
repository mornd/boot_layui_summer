package com.mornd.vo;

import com.mornd.entity.LoginUser;
import lombok.Data;

/**
 * @author mornd
 * @date 2020/12/29 - 18:20
 */
@Data
public class UserVo extends LoginUser {
    private Integer page;
    private Integer limit;

}
