package com.mornd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mornd.entity.Record;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author mornd
 * @date 2021/1/7 - 1:53
 */
@Mapper
public interface RecordMapper extends BaseMapper<Record> {
}
