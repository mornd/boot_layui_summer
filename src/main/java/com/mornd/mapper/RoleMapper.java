package com.mornd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mornd.entity.Menu;
import com.mornd.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * @author mornd
 * @date 2020/12/28 - 23:42
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 根据用户名查询角色列表
     * @param userId
     * @return
     */
    List<Role> findByUserId(Integer userId);


    /**
     * 根据角色id查询该角色下是否有对应的用户
     * @param roleId
     * @return
     */
    int checkRoleIfExistByUser(Integer roleId);

    /**
     * 根据角色id删除角色菜单表的关系
     * @param roleId
     * @return
     */
    int deleteRoleAndMenuRelation(Serializable roleId);

    /**
     * 根据角色id在关系表添加该角色的菜单
     * @param roleId
     * @param mids
     * @return
     */
    int addMenuByRoleId(@Param("roleId") Integer roleId,@Param("mids") List<Integer> mids);

    /**
     * 根据角色id查询对应的菜单id
     * @param RoleId
     * @return
     */
    List<Integer> findMenuByRoleId(Integer RoleId);
}
