package com.mornd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mornd.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.List;

/**
 * @author mornd
 * @date 2021/1/7 - 0:58
 * 菜单持久层
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    /**
     * 根据角色id查询菜单集合
     * @param roleId
     * @return
     */
    List<Menu> findByRoleId(Integer roleId);

    /**
     * 根据用户id查询该用户对应的角色
     * @param userId
     * @return
     */
    List<Menu> findByUserId(Integer userId);

    /**
     * 查询简单列
     * @return
     */
    List<Menu> getSimpleMenus();

    /**
     * 根据菜单id删除菜单角色关系表的数据
     * @param menuId
     * @return
     */
    int deleteRelationByMenuId(@Param("menuId") Serializable menuId);

    /**
     * 查看该菜单下是否有子菜单
     * @param menuId
     * @return
     */
    int checkHasChildrenNode(Integer menuId);


    /**
     * 超级管理员自动添加全部菜单权限
     * @param roleId
     * @param menuId
     * @return
     */
    int addMenuBySuperA(@Param("roleId") Integer roleId,@Param("menuId") Integer menuId);
}
