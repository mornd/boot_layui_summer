package com.mornd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mornd.entity.LoginUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author mornd
 * @date 2020/12/28 - 19:16
 */
@Mapper
public interface UserMapper extends BaseMapper<LoginUser> {
    /**
     * 根据用户名查询该用户的角色、权限列表
     * @param account
     * @return
     */
    LoginUser findUserByAccount(String account);


    /**
     * 根据用户id删除该用户在用户表与角色表中的关系
     * @param id
     * @return
     */
    int deleteUserAndRoleRelation(Integer id);

    /**
     * 根据用户id查询该用户的角色
     * @param id
     * @return
     */
    List<Map<String,Object>> findRoleByUserId(Integer id);

    /**
     * 根据用户id在关系表添加该用户的角色
     * @param userId
     * @param roles
     * @return
     */
    int addRolesByUserId(@Param("userId") Integer userId,@Param("roles") List<Integer> roles);

    /**
     * 删除该用户的所有角色信息
     * @param id
     * @return
     */
    int deleteRoleByUserId(Integer id);
}
