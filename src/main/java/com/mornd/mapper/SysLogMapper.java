package com.mornd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mornd.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author mornd
 * @date 2021/1/13 - 18:02
 * 日志持久层
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {
}
