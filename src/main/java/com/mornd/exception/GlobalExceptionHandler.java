package com.mornd.exception;

import com.alibaba.fastjson.JSON;
import com.mornd.result.AjaxResult;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * @author mornd
 * @date 2021/1/1 - 15:53
 * 全局异常处理类
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    /**
     * 全局异常处理
     * @param request
     * @param response
     * @param e
     * @return
     */
    @ExceptionHandler({Exception.class})
    public String handlerExceptions(HttpServletRequest request,HttpServletResponse response,
                                                        Exception e) {
        //未授权处理
        if (e instanceof UnauthorizedException) {
            return "redirect:/noAuthorization";
        }
        //其他异常
        return "redirect:/SysException";
    }
}

