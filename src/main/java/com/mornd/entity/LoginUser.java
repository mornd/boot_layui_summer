package com.mornd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mornd.entity.base.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * <p>
 * 
 * </p>
 *
 * @author mornd
 * @since 2020-12-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("s_user")
public class LoginUser extends DataEntity {
    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户真实姓名
     */
    private String realName;

    /**
     * 用户状态1可用/0禁用
     */
    private Integer status;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 性别1男/0女
     */
    private Integer gender;

    /**
     * 电话
     */
    private String phone;

    /**
     * 地址
     */
    private String address;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 是否在线 1在线/0离线
     */
    private Integer online;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 所属角色集合
     */
    @TableField(exist = false)
    Set<Role> roleSet;
}
