package com.mornd.entity;

import lombok.Data;

/**
 * @author mornd
 * @date 2020/12/28 - 21:15
 * 用户登录模型类
 */
@Data
public class LoginModel {
    private String username;
    private String password;
    private String imgVerify;

}
