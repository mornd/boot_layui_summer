package com.mornd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mornd.entity.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author mornd
 * @date 2021/1/13 - 16:41
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("s_sysLog")
public class SysLog implements Serializable {
    //标题
    private String title;
    //访问用户名
    private String userName;
    //类名
    private String className;
    //方法名
    private String method;
    //访问时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date visitTime;
    //操作时长
    private Long executionTime;
    //方法参数
    private String param;
    //IP地址
    private String ip;
    //请求URL
    private String url;
    //获取操作系统和浏览器信息
    private String osAndBrowser;
    //sessionId
    private String sessionId;
    //请求方式
    private String reqMethod;
    //是否是Ajax请求
    private String isAjax;
    //访问用户id
    private Integer userId;
    //方法返回值
    private String result;
    //异常信息
    private String exception;
}
