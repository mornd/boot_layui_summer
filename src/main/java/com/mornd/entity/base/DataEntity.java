package com.mornd.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author mornd
 * @date 2021/1/10 - 0:17
 */
@Data
public abstract class DataEntity extends BaseEntity {
    /**
     * 创建时间
     */
    //@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date gmtCreate;

    /**
     * 修改时间
     */
    private Date gmtModified;

    /**
     * 描述
     */
    private String description;
}
