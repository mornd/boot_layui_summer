package com.mornd.entity.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @author mornd
 * @date 2021/1/10 - 0:04
 */
@Data
public abstract class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 唯一id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
}
