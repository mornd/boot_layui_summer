package com.mornd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mornd.entity.base.BaseEntity;
import com.mornd.entity.base.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author mornd
 * @date 2020/12/29 - 17:09
 * 左侧菜单(权限)实体类
 */
@Data
@TableName("s_menu")
@EqualsAndHashCode(callSuper = false)
public class Menu extends DataEntity {
    //父id
    private Integer pid;
    //标题
    private String title;
    //类型
    private Integer type;
    //图标
    private String icon;
    //地址
    private String href;
    //打开类型
    private String openType;
    //子菜单集合
    @TableField(exist = false)
    private List<Menu> children;

    //custom
    //权限编码
    private String code;

    /**
     * 是否展开1:是0:否
     */
    private Integer spread;
}
