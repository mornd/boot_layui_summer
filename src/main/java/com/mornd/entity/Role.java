package com.mornd.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.mornd.entity.base.DataEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * <p>
 * 
 * </p>
 *
 * @author mornd
 * @since 2020-12-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("s_role")
public class Role extends DataEntity {
    /**
     * 角色code
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 所属菜单集合
     */
    @TableField(exist = false)
    private Set<Menu> menuSet;
}
