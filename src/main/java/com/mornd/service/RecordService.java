package com.mornd.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mornd.entity.Record;


/**
 * @author mornd
 * @date 2021/1/7 - 2:09
 */
public interface RecordService extends IService<Record> {
}
