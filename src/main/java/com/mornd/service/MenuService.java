package com.mornd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mornd.entity.Menu;
import com.mornd.vo.MenuVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author mornd
 * @date 2021/1/7 - 0:59
 */
public interface MenuService extends IService<Menu> {
    /**
     * 根据用户id查询该用户对应的角色
     * @param userId
     * @return
     */
    List<Menu> findByUserId(Integer userId);

    /**
     * 查询简单列
     * @return
     */
    List<Menu> getSimpleMenus();

    /**
     * 根据条件查询menu集合
     * @param menuVo
     * @return
     */
    IPage<Menu> findList(MenuVo menuVo);

    /**
     * 查看该菜单下是否有子菜单
     * @param menuId
     * @return
     */
    int checkHasChildrenNode(Integer menuId);

    /**
     * 超级管理员自动添加全部菜单权限
     * @param roleId
     * @param menuId
     * @return
     */
    int addMenuBySuperA(Integer roleId, Integer menuId);
}
