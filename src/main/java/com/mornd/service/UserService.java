package com.mornd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mornd.entity.LoginUser;
import com.mornd.vo.UserVo;

import java.util.List;
import java.util.Map;

/**
 * @author mornd
 * @date 2020/12/28 - 19:52
 */
public interface UserService extends IService<LoginUser> {
    /**
     * 根据用户名查询该用户的角色、权限列表
     * @param account
     * @return
     */
    LoginUser findUserByAccount(String account);

    /**
     * 查询用户列表
     * @param userVo
     * @return
     */
    IPage<LoginUser> findList(UserVo userVo);


    /**
     * 根据属性判断该值是否重复
     *
     * @param column 数据库的列名
     * @param field 属性名
     * @return
     */
    public LoginUser ifExistsByColumn(String column,Object field);

    /**
     * 根据用户id删除该用户在用户表与角色表中的关系
     * @param id
     * @return
     */
    boolean deleteUserAndRoleRelation(Integer id);

    /**
     * 根据用户id查询该用户的角色
     * @param id
     * @return
     */
    List<Map<String,Object>> findRoleByUserId(Integer id);

    /**
     * 根据用户名添加该用户的角色
     * @param userId
     * @param roles
     * @return
     */
    int addRolesByUserId(Integer userId,List<Integer> roles);

    /**
     * 删除该用户的所有角色信息
     * @param id
     * @return
     */
    int deleteRoleByUserId(Integer id);

    /**
     * 保存用户及用户对应的角色
     * @param entity
     * @param role
     * @return
     */
    Map saveAndRole(LoginUser entity,Integer[] role);

    /**
     * 编辑用户及对应的角色
     * @param user
     * @param role
     * @return
     */
    Map editAndRole(LoginUser user,Integer[] role);

    /**
     * 删除用户及对应的角色
     * @param id
     * @return
     */
    Map deleteAndRole(Integer id);

    /**
     * 修改用户状态
     * @param id
     * @param status
     * @return
     */
    Map updateStatus(Integer id, Integer status);

}
