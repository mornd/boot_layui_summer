package com.mornd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mornd.entity.SysLog;
import com.mornd.vo.SysLogVo;


/**
 * @author mornd
 * @date 2021/1/13 - 18:02
 */
public interface SysLogService extends IService<SysLog> {
    /**
     * 根据条件查询日志列表
     * @param logVo
     * @return
     */
    IPage<SysLog> findList(SysLogVo logVo);
}
