package com.mornd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mornd.entity.Menu;
import com.mornd.entity.Role;
import com.mornd.vo.RoleVo;

import java.io.Serializable;
import java.util.List;

/**操作角色业务
 * @author mornd
 * @date 2021/1/6 - 20:44
 */
public interface RoleService extends IService<Role> {
    /**
     * 查询角色集合
     * @param roleVo
     * @return
     */
    IPage<Role> findList(RoleVo roleVo);

    /**
     * 根据角色id查询该角色下是否有对应的用户
     * @param roleId
     * @return
     */
    int checkRoleIfExistByUser(Integer roleId);

    /**
     * 根据角色id在关系表添加该角色的菜单
     * @param roleId
     * @param mids
     * @return
     */
    int addMenuByRoleId(Integer roleId,List<Integer> mids);

    /**
     * 根据角色id查询对应的菜单
     * @param RoleId
     * @return
     */
    List<Integer> findMenuByRoleId(Integer RoleId);

    /**
     * 根据角色id删除角色菜单表的关系
     * @param roleId
     * @return
     */
    int deleteRoleAndMenuRelation(Serializable roleId);
}
