package com.mornd.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mornd.entity.Record;
import com.mornd.mapper.RecordMapper;
import com.mornd.service.RecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author mornd
 * @date 2021/1/7 - 2:09
 */
@Service
@Transactional
public class RecordServiceImpl extends ServiceImpl<RecordMapper, Record> implements RecordService {
}
