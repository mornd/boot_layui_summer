package com.mornd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mornd.entity.SysLog;
import com.mornd.mapper.SysLogMapper;
import com.mornd.service.SysLogService;
import com.mornd.vo.SysLogVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

/**
 * @author mornd
 * @date 2021/1/13 - 18:03
 * 日志业务层
 */
@Service
@Transactional(readOnly = true,rollbackFor = Exception.class)
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper,SysLog> implements SysLogService {
    /**
     * 根据条件查询日志列表
     *
     * @param logVo
     * @return
     */
    @Override
    public IPage<SysLog> findList(SysLogVo logVo) {
        IPage<SysLog> page = new Page<>(logVo.getPage(),logVo.getLimit());
        QueryWrapper<SysLog> wrapper = new QueryWrapper<>();
        //查询条件
        wrapper.like(!ObjectUtils.isEmpty(logVo.getTitle()),"title",logVo.getTitle());
        wrapper.like(!ObjectUtils.isEmpty(logVo.getUserName()),"user_name",logVo.getUserName());
        wrapper.like(!ObjectUtils.isEmpty(logVo.getClassName()),"class_name",logVo.getClassName());
        wrapper.orderByDesc("visit_time");
        return super.page(page,wrapper);
    }
}
