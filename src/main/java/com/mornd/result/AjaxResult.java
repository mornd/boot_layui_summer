package com.mornd.result;

import com.mornd.consts.ResultConst;

import java.util.HashMap;

/**
 * @author mornd
 * @date 2021/1/3 - 22:25
 * Ajax操作消息提醒
 */
public class AjaxResult extends HashMap<String,Object> {

    //操作是否正常执行
    private static final String SUCCESS = ResultConst.RESULT_SUCCESS;
    //返回的消息
    private static final String MESSAGE = ResultConst.RESULT_MESSAGE;
    //判断值是否存在
    private static final String EXIST = ResultConst.RESULT_EXIST;

    /**
     * 初始化对象
     */
    public AjaxResult() {
    }

    /**
     * 返回成功消息
     * @return
     */
    public static AjaxResult success(){
        return AjaxResult.success(true,"操作成功！");
    }

    /**
     * 返回成功消息
     * @param message 成功的消息
     * @return
     */
    public static AjaxResult success(String message){
        return success(true,message);
    }
    /**
     * 返回成功消息
     * @param condition
     * @param message
     * @return
     */
    public static AjaxResult success(boolean condition,String message){
        AjaxResult result = new AjaxResult();
        result.put(SUCCESS,condition);
        result.put(MESSAGE,message);
        return result;
    }


    /**
     * 返回成功消息
     * @param key 键值
     * @param value 内容
     * @return 成功消息
     */
    @Override
    public Object put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * 返回错误消息
     * @return
     */
    public static AjaxResult failure(){
        return failure(false,"操作失败，请重试！");
    }

    /**
     * 返回错误消息
     * @param message 错误信息
     * @return
     */
    public static AjaxResult failure(String message){
        return failure(false,message);
    }

    /**
     * 返回错误消息
     * @param condition
     * @param message
     * @return
     */
    public static AjaxResult failure(boolean condition,String message){
        AjaxResult result = new AjaxResult();
        result.put(SUCCESS,condition);
        result.put(MESSAGE,message);
        return result;
    }

    /**
     * 判断是否存在
     * @param condition
     * @return
     */
    public static AjaxResult isExists(boolean condition,String message){
        AjaxResult result = new AjaxResult();
        result.put(EXIST,condition);
        result.put(MESSAGE,message);
        return result;
    }
}
