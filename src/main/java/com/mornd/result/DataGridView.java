package com.mornd.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author mornd
 * @date 2020/12/29 - 18:09
 * 返回至前台的数据模型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataGridView<T> {
    private Integer code;
    private String msg;
    private Long count;
    private List<T> data;

    /**
     * 返回0代表成功
     * @param count
     * @param data
     */
    public DataGridView(Long count, List<T> data) {
        this.code = 0;
        this.msg = "success";
        this.count = count;
        this.data = data;
    }

    /**
     * 成功
     * @param count
     * @param data
     * @return
     */
    public DataGridView<T> success(Long count,List<T> data){
        this.code = 0;
        this.msg = "success";
        this.count = count;
        this.data = data;
        return this;
    }


    /**
     * 错误
     * @return
     */
    public DataGridView<T> failure(){
        return  failure(null,null);
    }

    /**
     * 错误
     * @param count
     * @param data
     * @return
     */
    public DataGridView<T> failure(Long count,List<T> data){
        this.code = 500;
        this.msg = "failure";
        this.count = count;
        this.data = data;
        return this;
    }
}
