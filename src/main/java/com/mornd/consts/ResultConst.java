package com.mornd.consts;

/**
 * @author mornd
 * @date 2021/1/3 - 23:14
 * 存储一些常量的接口
 */
public interface ResultConst {
    //返回的操作是否成功
    String RESULT_SUCCESS = "success";
    //返回的消息
    String RESULT_MESSAGE = "message";
    //判断值是否存在
    String RESULT_EXIST = "exist";
}
