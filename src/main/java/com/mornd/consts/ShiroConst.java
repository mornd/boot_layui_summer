package com.mornd.consts;

/**
 * @author mornd
 * @date 2021/1/4 - 21:47
 * 用户的常量接口
 */
public interface ShiroConst {
    //存入session登录用户key
    String LOGIN_USER = "loginUser";
    //加密方式
    String HASH_ALGORITHM_NAME = "MD5";
    //散列次数
    int HASH_ITERATIONS = 10;

    //用户权限code
    String R_USER = "R_USER";
    //管理员权限code
    String R_ADMIN = "R_ADMIN";
    //超级管理员code
    String R_SUPERADMIN = "R_SUPERADMIN";
}
