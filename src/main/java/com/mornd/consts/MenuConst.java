package com.mornd.consts;

/**
 * @author mornd
 * @date 2021/1/14 - 2:27
 * 菜单常量接口
 */
public interface MenuConst {
    /**
     * 根菜单父id
     */
    Integer TOP_ID = 0;
}
