package com.mornd.consts;

/**
 * @author mornd
 * @date 2021/1/10 - 1:01
 * 用户的实体类
 */
public interface UserConst {
    /**
     * 用户默认密码
     */
    String DEFAULT_PASSWORD = "000";
    /**
     * 用户禁用状态码
     */
    int DISABLED = 0;
    /**
     * 用户可用状态码
     */
    int USABLE = 1;

    /**
     * 超级管理员id
     */
    int SA_ID = 3;

    /**
     * 用户登录
     */
    int LOGIN_CODE = 1;

    /**
     * 用户退出
     */
    int LOGOUT_CODE = 0;
}
