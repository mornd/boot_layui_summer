package com.mornd.consts;

/**
 * @author mornd
 * @date 2021/1/13 - 15:07
 * 角色常量接口
 */
public interface RoleConst {
    /**
     * 角色编码前缀
     */
    String ROLE_PREFIX = "R_";
}
