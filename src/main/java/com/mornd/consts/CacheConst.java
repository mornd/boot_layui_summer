package com.mornd.consts;

/**
 * @author mornd
 * @date 2021/1/16 - 15:19
 * 缓存常量接口
 */
public interface CacheConst {
    //用户列表缓存名称
    String USER_CACHE_NAME = "userList_cache";
    //角色列表缓存名称
    String role_cache_name = "roleList_cache";
    //菜单列表缓存名称
    String menu_cache_name = "memuList_cache";
    //日志列表缓存名称
    String log_cache_name = "logList_cache";
}
