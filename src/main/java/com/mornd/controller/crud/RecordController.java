package com.mornd.controller.crud;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mornd.entity.Record;
import com.mornd.result.DataGridView;
import com.mornd.service.RecordService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author mornd
 * @date 2021/1/7 - 2:11
 */
@RestController
@RequestMapping("/record")
public class RecordController {
    @Resource
    private RecordService recordService;
    /**
     * 查询record集合
     * @return
     */
    @GetMapping("/getRecord")
    public DataGridView<Record> findRecord(@RequestParam(name = "page",defaultValue = "1") Integer page,@RequestParam(name = "limit",defaultValue = "5") Integer limit){
        IPage<Record> recordIPage = new Page<>(page,limit);
        QueryWrapper<Record> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("login_time");
        recordService.page(recordIPage,wrapper);
        return new DataGridView<Record>().success(recordIPage.getTotal(),recordIPage.getRecords());
    }
}
