package com.mornd.controller.crud;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mornd.entity.SysLog;
import com.mornd.result.DataGridView;
import com.mornd.service.SysLogService;
import com.mornd.vo.SysLogVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author mornd
 * @date 2021/1/13 - 18:44
 * 操作日志控制层
 */
@RestController
@RequestMapping("/log")
@RequiresPermissions({"m:log"})
public class SysLogController {
    @Resource
    private SysLogService sysLogService;

    /**
     * 查询日志列表
     * @param logVo
     * @return
     */
    @RequestMapping("/findLog")
    public DataGridView findLog(SysLogVo logVo){
        IPage<SysLog> list = sysLogService.findList(logVo);
        return new DataGridView<SysLog>().success(list.getTotal(),list.getRecords());
    }
}
