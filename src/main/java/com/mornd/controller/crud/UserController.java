package com.mornd.controller.crud;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mornd.annotation.LogStar;
import com.mornd.consts.ResultConst;
import com.mornd.consts.UserConst;
import com.mornd.result.AjaxResult;
import com.mornd.result.DataGridView;
import com.mornd.entity.LoginUser;
import com.mornd.service.UserService;
import com.mornd.shiro.ShiroPasswordUtil;
import com.mornd.vo.UserVo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author mornd
 * @date 2020/12/29 - 18:43
 */
@RestController
@RequestMapping("/user")
@RequiresPermissions({"m:user"})
public class UserController {
    @Resource
    private UserService userService;

    /**
     * 根据条件查询用户列表
     * @param userVo
     * @return
     */
    @LogStar("查询用户")
    @GetMapping("/findUser")
    public DataGridView<LoginUser> findUser(UserVo userVo){
        IPage<LoginUser> list = userService.findList(userVo);
        return new DataGridView<LoginUser>().success(list.getTotal(),list.getRecords());
    }

    /**
     * 根据id查询用户详情
     * @param id
     * @return
     */
    @PostMapping("/findById/{id}")
    public LoginUser  findById(@PathVariable("id") Integer id){
        QueryWrapper<LoginUser> user = new QueryWrapper<>();
        user.eq("id",id);
        return userService.getOne(user);
    }
    /**
     * 保存用户
     * @param user
     * @return
     */
    @LogStar("保存用户")
    @PostMapping("/save")
    public Map<String,Object> saveUser(LoginUser user,Integer[] role){
        return userService.saveAndRole(user,role);
    }

    /**
     * 修改用户
     * @param user
     * @return
     */
    @LogStar("编辑用户")
    @PostMapping("/edit")
    public Map editUser(LoginUser user,Integer[] role){
        return userService.editAndRole(user,role);
    }

    /**
     * 修改用户状态
     * @param status
     * @return
     */
    @PostMapping("/updateStatus")
    public Map updateStatus(Integer id, Integer status) {
        return userService.updateStatus(id,status);
    }

    /**
     * 用户删除
     * @param id
     * @return
     */
    @LogStar("删除用户")
    @PostMapping("/delete/{id}")
    public Map deleteUser(@PathVariable("id") Integer id){
        return userService.deleteAndRole(id);
    }

    /**
     * 根据用户id查询该用户的角色信息
     * @param id
     * @return
     */
    @PostMapping("/findRoleById/{id}")
    public List<Map<String, Object>> findRoleByUserId(@PathVariable("id") Integer id){
        return userService.findRoleByUserId(id);
    }
}
