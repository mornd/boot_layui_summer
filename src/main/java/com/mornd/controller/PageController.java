package com.mornd.controller;

import com.mornd.consts.MenuConst;
import com.mornd.entity.LoginUser;
import com.mornd.entity.Menu;
import com.mornd.utils.TreeMenuUtil;
import com.mornd.service.MenuService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author mornd
 * @date 2020/12/27 - 17:50
 * 页面跳转控制器
 */
@Controller
public class PageController {
    @Resource
    private MenuService menuService;

    /**
     * 加载左侧菜单该菜单(所有人可访问)
     */
    @ResponseBody
    @RequestMapping("/getMenuByUserId")
    public List<Menu> getMenu(){
        //数据库中读取的菜单集合
        LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        List<Menu> menuList = menuService.findByUserId(loginUser.getId());
        return TreeMenuUtil.toTree(menuList, MenuConst.TOP_ID);
    }

    /**
     * 获取主页控制台页面
     * @return
     */
    @RequiresPermissions({"m:console"})
    @GetMapping("/getConsole")
    public String getConsole(){
        return "view/notice/console";
    }

    /**
     * 获取登录用户信息
     * @return
     */
    @RequestMapping("/user/getInfo")
    public String getUserInfo(){
        return "view/main/basicInfo";
    }
    /**
     * 跳至用户列表页面
     * @return
     */
    @RequestMapping("/user/toUserList")
    @RequiresPermissions({"m:user"})
    public String toUserList(){
        return "view/system/user";
    }

    /**
     * 跳至角色列表页面
     * @return
     */
    @RequestMapping("/role/toRoleList")
    @RequiresPermissions({"m:role"})
    public String toRoleList(){
        return "view/system/role";
    }

    /**
     * 跳至菜单列表页面
     * @return
     */
    @RequestMapping("/menu/toMenuList")
    @RequiresPermissions({"m:menu"})
    public String toMenuList(){
        return "view/system/menu";
    }

    /**
     * 跳至日志列表页面
     * @return
     */
    @RequestMapping("/log/toLogList")
    @RequiresPermissions({"m:log"})
    public String toLogList(){
        return "view/system/log";
    }
}
