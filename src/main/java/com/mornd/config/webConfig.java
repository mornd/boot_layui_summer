package com.mornd.config;

import com.mornd.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author mornd
 * @date 2020/12/28 - 19:12
 */
@Configuration
public class webConfig implements WebMvcConfigurer {
    /**
     * 设置不拦截的页面
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        //访问根目录为登录页面
        registry.addViewController("/").setViewName("login");
        registry.addViewController("/index.html").setViewName("login");
        //跳至主页面
        registry.addViewController("/main").setViewName("index");
        //未授权
        registry.addViewController("/noAuthorization").setViewName("error/403");
        //系统异常
        registry.addViewController("/SysException").setViewName("error/5xx");
    }

    /**
     * 注册拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/**")
                    .excludePathPatterns("/","/index.html","/api/login","/api/getVerify","/api/getCaptchaVoice","/pear_layui/**");
    }

    /**
     * 跨域请求
     * @param registry
     */
    /*@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")//允许所有的访问请求（访问路径）
                .allowedMethods("*")//允许所有的请求方法访问该跨域资源服务器
                .allowedOrigins("*")//允许所有的请求域名访问我们的跨域资源
                .allowedHeaders("*")//允许所有的请求header访问
                //.allowCredentials(true)
                //.allowedMethods("GET","POST","DELETE","PUT","PATCH")
                .maxAge(3600);
    }*/
}
