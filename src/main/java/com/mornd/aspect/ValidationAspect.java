package com.mornd.aspect;

import com.mornd.result.AjaxResult;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * @author mornd
 * @date 2021/1/14 - 15:33
 * 非空验证切面
 */
@Aspect
@Order(1)
@Component
public class ValidationAspect {
    @Pointcut("execution(* com.mornd.controller.crud..*.save*(..))")
    public void save(){

    }
    @Pointcut("execution(* com.mornd.controller.crud..*.delete*(..))")
    public void delete(){
    }

    /**
     * 删除环绕通知
     * @param pjp
     * @return
     */
    @Around("delete()")
    public Object ValidationDelete(ProceedingJoinPoint pjp) {
        Object[] args = pjp.getArgs();
        for (int i = 0; i < args.length; i++) {
            if(ObjectUtils.isEmpty(args[i])){
                return AjaxResult.failure("提交数据为空");
            }
        }
        try {
            return pjp.proceed(args);
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
