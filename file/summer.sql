/*
SQLyog Ultimate v11.27 (32 bit)
MySQL - 8.0.19 : Database - db_springboot_summer
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_springboot_summer` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_springboot_summer`;

/*Table structure for table `s_menu` */

DROP TABLE IF EXISTS `s_menu`;

CREATE TABLE `s_menu` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int DEFAULT '0' COMMENT '父节点id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `type` int DEFAULT NULL COMMENT '菜单类型',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `href` varchar(100) DEFAULT NULL COMMENT '路径',
  `open_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '打开方式',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单编码',
  `spread` int DEFAULT '0' COMMENT '是否展开1:是0:否',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `s_menu` */

insert  into `s_menu`(`id`,`pid`,`title`,`type`,`icon`,`href`,`open_type`,`code`,`spread`,`gmt_create`,`gmt_modified`,`description`) values (1,0,'工作空间',0,'layui-icon layui-icon-console','/','','m:work',1,'2021-01-11 17:04:37','2021-01-13 20:22:04',''),(2,0,'系统管理',0,'layui-icon layui-icon-set-fill','/','','m:system',1,'2021-01-11 17:04:37','2021-01-13 13:47:13',''),(3,1,'系统主页',1,'layui-icon layui-icon-home','/getConsole','_iframe','m:console',0,'2021-01-11 17:04:37','2021-01-13 01:29:56',''),(4,1,'系统公告',1,'layui-icon layui-icon-notice','/getNotive','_iframe','m:notive',0,'2021-01-11 17:04:37','2021-01-13 01:31:41',''),(5,2,'用户管理',1,'layui-icon layui-icon-friends','/user/toUserList','_iframe','m:user',0,'2021-01-11 17:04:37','2021-01-13 01:30:09',''),(6,2,'角色管理',1,'layui-icon layui-icon-password','/role/toRoleList','_iframe','m:role',0,'2021-01-11 17:04:37','2021-01-13 01:30:52',''),(7,2,'菜单管理',1,'layui-icon layui-icon-form','/menu/toMenuList','_iframe','m:menu',0,'2021-01-11 17:04:37','2021-01-13 01:30:34',''),(8,2,'日志管理',1,'layui-icon layui-icon-camera-fill','/log/toLogList','_iframe','m:log',0,'2021-01-11 17:04:37','2021-01-13 01:27:22',''),(31,2,'test',1,'layui-icon layui-icon-tree','/','_iframe','m:test',0,'2021-01-13 13:52:52',NULL,'');

/*Table structure for table `s_record` */

DROP TABLE IF EXISTS `s_record`;

CREATE TABLE `s_record` (
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `logout_time` datetime DEFAULT NULL COMMENT '登出时间',
  `account` varchar(50) DEFAULT NULL COMMENT '登录账号',
  `real_name` varchar(50) DEFAULT NULL COMMENT '登录姓名',
  `online` int DEFAULT NULL COMMENT '是否在线',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `s_record` */

insert  into `s_record`(`login_time`,`logout_time`,`account`,`real_name`,`online`,`avatar`) values ('2021-01-07 01:49:47','2021-01-07 01:49:50','tom','tom',1,NULL),('2021-01-07 02:01:50','2021-01-07 02:01:52','jetty','jetty',0,NULL);

/*Table structure for table `s_role` */

DROP TABLE IF EXISTS `s_role`;

CREATE TABLE `s_role` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色code',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime DEFAULT NULL,
  `description` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `s_role` */

insert  into `s_role`(`id`,`code`,`name`,`gmt_create`,`gmt_modified`,`description`) values (1,'R_USER','用户','2021-01-01 22:11:23','2021-01-12 17:02:18','用户只有基本的一些权限'),(2,'R_ADMIN','管理员','2021-01-01 22:11:37','2021-01-12 18:26:19','管理员拥有所有菜单权限'),(3,'R_SUPERADMIN','超级管理员','2021-01-02 22:11:41','2021-01-13 13:19:31','超级管理员拥有自动添加所有权限，并且不可删除'),(28,'R_TEST','test','2021-01-13 13:35:51','2021-01-13 20:47:02','');

/*Table structure for table `s_role_menu` */

DROP TABLE IF EXISTS `s_role_menu`;

CREATE TABLE `s_role_menu` (
  `r_id` int DEFAULT NULL COMMENT '角色id',
  `m_id` int DEFAULT NULL COMMENT '菜单id',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  KEY `r_id` (`r_id`),
  KEY `m_id` (`m_id`),
  CONSTRAINT `s_role_menu_ibfk_1` FOREIGN KEY (`r_id`) REFERENCES `s_role` (`id`),
  CONSTRAINT `s_role_menu_ibfk_2` FOREIGN KEY (`m_id`) REFERENCES `s_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `s_role_menu` */

insert  into `s_role_menu`(`r_id`,`m_id`,`createDate`,`description`) values (1,1,'2021-01-12 17:02:17',NULL),(1,3,'2021-01-12 17:02:17',NULL),(1,4,'2021-01-12 17:02:17',NULL),(2,1,'2021-01-12 18:26:19',NULL),(2,3,'2021-01-12 18:26:19',NULL),(2,4,'2021-01-12 18:26:19',NULL),(2,2,'2021-01-12 18:26:19',NULL),(2,5,'2021-01-12 18:26:19',NULL),(2,6,'2021-01-12 18:26:19',NULL),(2,7,'2021-01-12 18:26:19',NULL),(2,8,'2021-01-12 18:26:19',NULL),(3,1,'2021-01-13 13:19:30',NULL),(3,3,'2021-01-13 13:19:30',NULL),(3,4,'2021-01-13 13:19:30',NULL),(3,2,'2021-01-13 13:19:30',NULL),(3,5,'2021-01-13 13:19:30',NULL),(3,6,'2021-01-13 13:19:30',NULL),(3,7,'2021-01-13 13:19:30',NULL),(3,8,'2021-01-13 13:19:30',NULL),(3,31,'2021-01-13 13:52:52',NULL);

/*Table structure for table `s_syslog` */

DROP TABLE IF EXISTS `s_syslog`;

CREATE TABLE `s_syslog` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `title` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作用户',
  `class_name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '类名',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '方法名',
  `param` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '方法参数',
  `visit_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '访问时间',
  `execution_time` bigint DEFAULT NULL COMMENT '操作时长',
  `ip` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ip',
  `url` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'url',
  `os_and_browser` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '操作系统及浏览器信息',
  `session_id` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'session id',
  `req_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '请求方式',
  `is_ajax` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否是Ajax请求',
  `user_id` int DEFAULT '0' COMMENT '操作用户id',
  `result` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '方法返回结果',
  `exception` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '异常信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/*Data for the table `s_syslog` */

insert  into `s_syslog`(`id`,`title`,`user_name`,`class_name`,`method`,`param`,`visit_time`,`execution_time`,`ip`,`url`,`os_and_browser`,`session_id`,`req_method`,`is_ajax`,`user_id`,`result`,`exception`) values (35,'登录','tom','com.mornd.controller.LoginController','login','[\"org.apache.shiro.web.servlet.ShiroHttpServletRequest@153abd27\",{\"imgVerify\":\"\",\"password\":\"000\",\"username\":\"tom\"}]','2021-01-13 20:22:43',63,'127.0.0.1','http://localhost:8000/api/login','Windows - Chrome-87.0.4280.141','44e392e7-cc87-4bbe-af6f-cd1eea5ab20c','POST','是',1,'{\"success\":true}',NULL),(36,'查询用户','tom','com.mornd.controller.crud.UserController','findUser','[{\"limit\":8,\"page\":1}]','2021-01-13 20:23:00',47,'127.0.0.1','http://localhost:8000/user/findUser','Windows - Chrome-87.0.4280.141','44e392e7-cc87-4bbe-af6f-cd1eea5ab20c','GET','是',1,'结果过长,不予显示',NULL),(37,'查询角色','tom','com.mornd.controller.crud.RoleController','findRole','[{\"limit\":8,\"page\":1}]','2021-01-13 20:23:29',31,'127.0.0.1','http://localhost:8000/role/findRole','Windows - Chrome-87.0.4280.141','44e392e7-cc87-4bbe-af6f-cd1eea5ab20c','GET','是',1,'结果过长,不予显示',NULL),(38,'登录','tom','com.mornd.controller.LoginController','login','[\"org.apache.shiro.web.servlet.ShiroHttpServletRequest@ec305d9\",{\"imgVerify\":\"\",\"password\":\"000\",\"username\":\"tom\"}]','2021-01-13 20:43:42',47,'127.0.0.1','http://localhost:8000/api/login','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','POST','是',1,'{\"success\":true}',NULL),(39,'查询菜单','tom','com.mornd.controller.crud.MenuController','findMenu','[{\"limit\":8,\"page\":1}]','2021-01-13 20:43:47',15,'127.0.0.1','http://localhost:8000/menu/findMenu','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','GET','是',1,'结果过长,不予显示',NULL),(40,'查询角色','tom','com.mornd.controller.crud.RoleController','findRole','[{\"limit\":8,\"page\":1}]','2021-01-13 20:45:13',28,'127.0.0.1','http://localhost:8000/role/findRole','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','GET','是',1,'结果过长,不予显示',NULL),(41,'编辑角色','tom','com.mornd.controller.crud.RoleController','editRole','[{\"code\":\"R_TEST\",\"description\":\"\",\"id\":28,\"name\":\"test\"},\"\"]','2021-01-13 20:45:18',30,'127.0.0.1','http://localhost:8000/role/edit','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','POST','是',1,'{\"success\":true,\"message\":\"角色修改成功\"}',NULL),(42,'查询角色','tom','com.mornd.controller.crud.RoleController','findRole','[{\"limit\":8,\"page\":1}]','2021-01-13 20:45:18',12,'127.0.0.1','http://localhost:8000/role/findRole','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','GET','是',1,'结果过长,不予显示',NULL),(43,'查询用户','tom','com.mornd.controller.crud.UserController','findUser','[{\"limit\":8,\"page\":1}]','2021-01-13 20:45:22',28,'127.0.0.1','http://localhost:8000/user/findUser','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','GET','是',1,'结果过长,不予显示',NULL),(44,'查询用户','tom','com.mornd.controller.crud.UserController','findUser','[{\"limit\":8,\"page\":1}]','2021-01-13 20:45:24',22,'127.0.0.1','http://localhost:8000/user/findUser','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','GET','是',1,'结果过长,不予显示',NULL),(45,'查询用户','tom','com.mornd.controller.crud.UserController','findUser','[{\"limit\":8,\"page\":1}]','2021-01-13 20:45:25',25,'127.0.0.1','http://localhost:8000/user/findUser','Windows - Chrome-87.0.4280.141','fafc18cd-87fc-4016-9bca-6460c6bc7684','GET','是',1,'结果过长,不予显示',NULL),(46,'登录','tom','com.mornd.controller.LoginController','login','[\"org.apache.shiro.web.servlet.ShiroHttpServletRequest@1ddcb2e\",{\"imgVerify\":\"\",\"password\":\"000\",\"username\":\"tom\"}]','2021-01-13 20:46:47',38,'127.0.0.1','http://localhost:8000/api/login','Windows - Chrome-87.0.4280.141','e58eb082-5c35-4363-92b7-a8db185f662b','POST','是',1,'{\"success\":true}',NULL),(47,'查询菜单','tom','com.mornd.controller.crud.MenuController','findMenu','[{\"limit\":8,\"page\":1}]','2021-01-13 20:46:50',32,'127.0.0.1','http://localhost:8000/menu/findMenu','Windows - Chrome-87.0.4280.141','e58eb082-5c35-4363-92b7-a8db185f662b','GET','是',1,'结果过长,不予显示',NULL),(48,'查询角色','tom','com.mornd.controller.crud.RoleController','findRole','[{\"limit\":8,\"page\":1}]','2021-01-13 20:46:59',38,'127.0.0.1','http://localhost:8000/role/findRole','Windows - Chrome-87.0.4280.141','e58eb082-5c35-4363-92b7-a8db185f662b','GET','是',1,'结果过长,不予显示',NULL),(49,'编辑角色','tom','com.mornd.controller.crud.RoleController','editRole','[{\"code\":\"R_TEST\",\"description\":\"\",\"id\":28,\"name\":\"test\"},\"\"]','2021-01-13 20:47:02',31,'127.0.0.1','http://localhost:8000/role/edit','Windows - Chrome-87.0.4280.141','e58eb082-5c35-4363-92b7-a8db185f662b','POST','是',1,'{\"success\":true,\"message\":\"角色修改成功\"}',NULL),(50,'查询角色','tom','com.mornd.controller.crud.RoleController','findRole','[{\"limit\":8,\"page\":1}]','2021-01-13 20:47:02',14,'127.0.0.1','http://localhost:8000/role/findRole','Windows - Chrome-87.0.4280.141','e58eb082-5c35-4363-92b7-a8db185f662b','GET','是',1,'结果过长,不予显示',NULL);

/*Table structure for table `s_user` */

DROP TABLE IF EXISTS `s_user`;

CREATE TABLE `s_user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `account` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `real_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户真实姓名',
  `status` int DEFAULT '1' COMMENT '用户状态1可用/0禁用',
  `age` int DEFAULT NULL COMMENT '年龄',
  `gender` int DEFAULT '1' COMMENT '性别1男/0女',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '电话',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '地址',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT NULL COMMENT '修改时间',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像',
  `online` int DEFAULT '1' COMMENT '是否在线 1：在线，0：退出',
  `salt` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '盐值',
  PRIMARY KEY (`id`,`real_name`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Data for the table `s_user` */

insert  into `s_user`(`id`,`account`,`password`,`real_name`,`status`,`age`,`gender`,`phone`,`address`,`gmt_create`,`gmt_modified`,`description`,`avatar`,`online`,`salt`) values (1,'tom','6746b95dcda0a5b1e31408010142ef2e','tom',1,20,1,'18270917788','广东省广州市','2020-12-27 19:44:09','2021-01-10 01:47:40','',NULL,1,'30f2eb577111747d866b021a2a563b7e'),(2,'jetty','3913ba70f30e464171579c4e4b32b6b0','jetty',1,18,0,'18262547895','广东省广州市','2020-12-28 20:39:25',NULL,NULL,NULL,1,'0df0d8b8eafebd3c1e21d05c283da7de'),(3,'jack','3913ba70f30e464171579c4e4b32b6b0','jack',1,20,1,'15245625325','广东省广州市','2020-12-30 17:47:00','2021-01-13 12:50:35','',NULL,0,'0df0d8b8eafebd3c1e21d05c283da7de'),(4,'cindy','3913ba70f30e464171579c4e4b32b6b0','cindy',0,18,0,'12532567841','江西省九江市','2020-12-30 17:47:38','2021-01-13 15:04:28','',NULL,1,'0df0d8b8eafebd3c1e21d05c283da7de'),(5,'bob','3913ba70f30e464171579c4e4b32b6b0','bob',1,18,1,'15232561548','上海市','2020-12-30 17:48:20','2021-01-11 17:42:55','',NULL,1,'0df0d8b8eafebd3c1e21d05c283da7de'),(13,'alan','3913ba70f30e464171579c4e4b32b6b0','alan',1,20,1,'15245685214','北京市','2021-01-01 23:39:18','2021-01-13 15:03:59','这个人很乐观！',NULL,1,'0df0d8b8eafebd3c1e21d05c283da7de'),(14,'zhangsan','3913ba70f30e464171579c4e4b32b6b0','张三',1,18,1,'15245124856','上海市','2021-01-02 20:28:04','2021-01-05 15:26:26','',NULL,1,'0df0d8b8eafebd3c1e21d05c283da7de'),(19,'lisi','3913ba70f30e464171579c4e4b32b6b0','李四',1,21,1,'11542562514','北京市','2021-01-06 18:30:03','2021-01-11 17:46:09','',NULL,1,'0df0d8b8eafebd3c1e21d05c283da7de'),(26,'wangwu','36a8019d306342fd84a36622c107f01b','王五',1,22,1,'15263254854','北京市','2021-01-13 13:12:44','2021-01-13 13:14:27','',NULL,1,'259a1c7a7240b0a9c7dd4a3b54eae17c'),(27,'12','8ae6e3e9b33ab29bbaf3de28c455b941','12',1,12,1,'11111111111','12','2021-01-13 19:23:07',NULL,'',NULL,1,'3e9659181f5dc422496e09d7492f1ee4');

/*Table structure for table `s_user_role` */

DROP TABLE IF EXISTS `s_user_role`;

CREATE TABLE `s_user_role` (
  `u_id` int DEFAULT NULL COMMENT '用户id',
  `r_id` int DEFAULT NULL COMMENT '角色id',
  `createDate` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `description` varchar(50) DEFAULT NULL COMMENT '描述',
  KEY `user_role1` (`u_id`),
  KEY `user_role2` (`r_id`),
  CONSTRAINT `user_role1` FOREIGN KEY (`u_id`) REFERENCES `s_user` (`id`),
  CONSTRAINT `user_role2` FOREIGN KEY (`r_id`) REFERENCES `s_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `s_user_role` */

insert  into `s_user_role`(`u_id`,`r_id`,`createDate`,`description`) values (2,1,'2020-12-28 20:39:57',NULL),(1,2,'2021-01-10 01:47:40',NULL),(5,1,'2021-01-11 17:42:54',NULL),(3,3,'2021-01-13 12:50:34',NULL),(26,1,'2021-01-13 13:14:26',NULL),(27,1,'2021-01-13 19:23:07',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
